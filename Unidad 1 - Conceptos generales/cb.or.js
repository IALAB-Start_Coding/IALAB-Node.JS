const sayHello = () => console.log("Hello World!");

setTimeout(sayHello, 2000);

// Ejemplo calculate

const calculate = (arg1, arg2, operation) => {
  console.log("Calculando...");
  return console.log(operation(arg1, arg2));
};

const sum = (num1, num2) => num1 + num2;

const multiply = (num1, num2) => num1 * num2;

calculate(5, 2, multiply);
