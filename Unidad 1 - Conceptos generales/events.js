function Emiiter() {
  this.events = {};
}

Emiiter.prototype.on = function (type, listener) {
  this.events[type] = this.events[type] || [];
  this.events[type].push(listener);
};

Emiiter.prototype.emit = function (type, opts) {
  if (this.events[type]) {
    this.events[type].forEach((listener) => listener(opts));
  }
};

module.exports = Emiiter;
