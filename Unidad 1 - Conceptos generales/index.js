const Emiiter = require("events");

const emiiter = new Emiiter();

emiiter.on("save", ({ date }) => {
  console.log(" *** Event fired ***", date);
});

setInterval(() => {
  emiiter.emit("save", { date: Date.now() });
}, 1000);
