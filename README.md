# **Curso IALAB - Start Coding - Node.JS**

Curso de Inteligencia Artificial - Start Coding - Node.JS

## **Introduccion**

- Informacion general

## **Unidad 1: Conceptos generales**

1. Javascript y NodeJS
2. Javascript en el servidor
3. Motor V8
4. Sincronía y asincronía
5. Eventos & Event Emitter
6. Bucle de eventos y operaciones Blocking vs Non Blocking
7. Callbacks
8. Promesas
9. Async / Await

## **Unidad 2: Construyendo nuestra app**

1. Inicializando e instalando dependecias
2. Instalando dependencias de desarrollo
3. Configurando Typescript
4. Creando nuestro servidor Http
5. Configurando Apollo Server
6. Configurando nuestra base de datos postgres
7. Creando nuestras entities
8. Resolvers, parte 1
9. Resolvers, parte 2
10. Creando nuestro CRUD de Autores
11. Creando nuestro CRUD de Libros
12. Creando nuestro CRUD de Libros parte 2
13. Probando los CRUD's
14. Aplicando validaciones
15. Registrando al usuario. JWT. Estrategia
16. Inicio de sesión
17. Autenticando las peticiones
18. Creando nuestro repo. Git & Github
19. Haciendo arreglos a nuestro código. Refactoring

## **Repositorio GitHub**

- Repositorio Github

## **Tutoría Sincrónica**

- Segunda tutoría sincrónica: Node.js

## **Challenge**

- Biblioteca virtual. Explicación
